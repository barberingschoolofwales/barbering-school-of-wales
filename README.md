We offer fast-track training courses to aspiring barbers, employers who wish to further train their staff, and business owners interested in keeping up to speed on the latest techniques in the barber industry. The courses include NVQ level 2 barber course, NVQ hairdresser to barber and much more. 

To register your interest in a course call us at 07800 60 3365 or fill out the form on the website.

Website: https://barberschoolwales.co.uk/
